import HelloWorld from "./HelloWorld.jsx";
import { Component } from "react";

class App extends Component {
  render() {
    return <HelloWorld />;
  }
}

export default App;
